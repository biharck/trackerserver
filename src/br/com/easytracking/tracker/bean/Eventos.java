package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="eventos")
public class Eventos{

	private Integer id;
	private Veiculo veiculo;
	private String menssagem;
	private String dataEvento;
	private boolean visto;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	@ForeignKey(name="fk_eventos_veiculo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public String getMenssagem() {
		return menssagem;
	}
	public String getDataEvento() {
		return dataEvento;
	}
	
	public boolean isVisto() {
		return visto;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setMenssagem(String menssagem) {
		this.menssagem = menssagem;
	}
	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}
	public void setVisto(boolean visto) {
		this.visto = visto;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
