package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="coordenada")
public class Coordenadas {
	
	private Integer id;
	private String serial;
	private String transmissionReason;
	private String date;
	private String latitude;
	private String longitude;
	private String course;
	private String speed;
	private String csq;
	private String svn;
	private String snr;
	private String gprsConnection;
	private String gpsSignal;
	private String gpsAntennaFailure;
	private String gpsAntennaDisconnected;
	private String excessSpeed;
	private String gpsSleep;
	private String gsmJamming;
	private String moving;
	private String temperature;
	private String input1;
	private String panic;
	private String output1;
	private String antiTheftStatus;
	private String batteryCharging;
	private String batteryFailure;
	
	public Coordenadas(){};	
	
	public Coordenadas(String serial, String transmissionReason, String date,
			String latitude, String longitude, String course, String speed,
			String csq, String svn, String snr, String gprsConnection,
			String gpsSignal, String gpsAntennaFailure, String gpsAntennaDisconnected,
			String excessSpeed,String gpsSleep, String gsmJamming, String moving,
			String temperature, String input1, String panic, String output1,
			String antiTheftStatus, String batteryCharging,
			String batteryFailure) {

		this.serial = serial;
		this.transmissionReason = transmissionReason;
		this.date = date;
		this.latitude = latitude;
		this.longitude = longitude;
		this.course = course;
		this.speed = speed;
		this.csq = csq;
		this.svn = svn;
		this.snr = snr;
		this.gprsConnection = gprsConnection;
		this.gpsSignal = gpsSignal;
		this.gpsAntennaFailure = gpsAntennaFailure;
		this.gpsAntennaDisconnected = gpsAntennaDisconnected;
		this.excessSpeed = excessSpeed;
		this.gpsSleep = gpsSleep;
		this.gsmJamming = gsmJamming;
		this.moving = moving;
		this.temperature = temperature;
		this.input1 = input1;
		this.panic = panic;
		this.output1 = output1;
		this.antiTheftStatus = antiTheftStatus;
		this.batteryCharging = batteryCharging;
		this.batteryFailure = batteryFailure;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTransmissionReason() {
		return transmissionReason;
	}
	public void setTransmissionReason(String transmissionReason) {
		this.transmissionReason = transmissionReason;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getCsq() {
		return csq;
	}
	public void setCsq(String csq) {
		this.csq = csq;
	}
	public String getSvn() {
		return svn;
	}
	public void setSvn(String svn) {
		this.svn = svn;
	}
	public String getSnr() {
		return snr;
	}
	public void setSnr(String snr) {
		this.snr = snr;
	}
	public String getGprsConnection() {
		return gprsConnection;
	}
	public void setGprsConnection(String gprsConnection) {
		this.gprsConnection = gprsConnection;
	}
	public String getGpsSignal() {
		return gpsSignal;
	}
	public void setGpsSignal(String gpsSignal) {
		this.gpsSignal = gpsSignal;
	}
	public String getGpsAntennaFailure() {
		return gpsAntennaFailure;
	}
	public String getGpsAntennaDisconnected() {
		return gpsAntennaDisconnected;
	}
	public void setGpsAntennaDisconnected(String gpsAntennaDisconnected) {
		this.gpsAntennaDisconnected = gpsAntennaDisconnected;
	}
	public void setGpsAntennaFailure(String gpsAntennaFailure) {
		this.gpsAntennaFailure = gpsAntennaFailure;
	}
	public String getExcessSpeed() {
		return excessSpeed;
	}
	public void setExcessSpeed(String excessSpeed) {
		this.excessSpeed = excessSpeed;
	}
	public String getGpsSleep() {
		return gpsSleep;
	}
	public void setGpsSleep(String gpsSleep) {
		this.gpsSleep = gpsSleep;
	}
	public String getGsmJamming() {
		return gsmJamming;
	}
	public void setGsmJamming(String gsmJamming) {
		this.gsmJamming = gsmJamming;
	}
	public String getMoving() {
		return moving;
	}
	public void setMoving(String moving) {
		this.moving = moving;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getInput1() {
		return input1;
	}
	public void setInput1(String input1) {
		this.input1 = input1;
	}
	public String getPanic() {
		return panic;
	}
	public void setPanic(String panic) {
		this.panic = panic;
	}
	public String getOutput1() {
		return output1;
	}
	public void setOutput1(String output1) {
		this.output1 = output1;
	}
	public String getAntiTheftStatus() {
		return antiTheftStatus;
	}
	public void setAntiTheftStatus(String antiTheftStatus) {
		this.antiTheftStatus = antiTheftStatus;
	}
	public String getBatteryCharging() {
		return batteryCharging;
	}
	public void setBatteryCharging(String batteryCharging) {
		this.batteryCharging = batteryCharging;
	}
	public String getBatteryFailure() {
		return batteryFailure;
	}
	public void setBatteryFailure(String batteryFailure) {
		this.batteryFailure = batteryFailure;
	}
}
