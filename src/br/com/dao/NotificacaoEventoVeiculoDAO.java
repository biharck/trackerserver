package br.com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.tracker.bean.NotificacaoEventoVeiculo;
import br.com.util.HibernateUtil;

public class NotificacaoEventoVeiculoDAO extends GenericDAO<NotificacaoEventoVeiculo>{
	
	public NotificacaoEventoVeiculo findById(Integer id){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		NotificacaoEventoVeiculo b = null;
		
		try {
			transaction = session.beginTransaction();
			b = (NotificacaoEventoVeiculo) session.createQuery("from NotificacaoEventoVeiculo WHERE veiculo = "+id).uniqueResult();
			
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return b;
	}

}
