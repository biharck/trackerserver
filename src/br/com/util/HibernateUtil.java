package br.com.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Coordenadas;
import br.com.easytracking.tracker.bean.Eventos;
import br.com.easytracking.tracker.bean.NotificacaoEventoVeiculo;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.Veiculo;


public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new AnnotationConfiguration()
								.configure()
								.addPackage("br.com.easytracking.tracker.bean") //the fully qualified package name
								.addAnnotatedClass(Coordenadas.class)
								.addAnnotatedClass(Rastreador.class)
								.addAnnotatedClass(Veiculo.class)
								.addAnnotatedClass(Cliente.class)
								.addAnnotatedClass(NotificacaoEventoVeiculo.class)
								.addAnnotatedClass(Eventos.class)
								.buildSessionFactory();

		} catch (Throwable ex) {
			System.err.println("Cria��o do SessionFactory falhou:." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
