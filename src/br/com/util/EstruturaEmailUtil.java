package br.com.util;

public class EstruturaEmailUtil {
	
	public static String getEmailVelocidadeLimite(String placa, String cliente){
		StringBuilder sb = new StringBuilder();
		sb.append("<span style='font-size:20px; color: #2281CF;'>Prezado(a) "+cliente+",</span>");
		sb.append(		"<hr style='border: 1px #CDCDCD solid;'>");
		sb.append("<span style='font-size:12px; color:#555;'>");
		sb.append("	Foi identificado que o ve&iacute;culo placa <font style='color:#E77272'>"+placa+" </font>est&aacute; acima do limite de velocidade configurado por voc&ecirc;.");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<i>");
		sb.append("Cordialmente,");
		sb.append("<div>");
		sb.append("	<div>");
		sb.append("		<div>");
		sb.append("			<span style='font-size:x-small'>");
		sb.append("				<font color='#336666'><img src='http://www.trackhost.srv.br/Tracker/img/topo_login.png' width='96' height='49'><br></font>");
		sb.append("			</span>");
		sb.append("			<font color='#003333'>");
		sb.append("				<span style='font-size:x-small'>&nbsp;&nbsp;<a href='http://www.easytracking.com.br' target='_blank'>http://www.easytracking.com.br</a></span>");
		sb.append("			</font><br>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("</div>");
		sb.append("</i>");
		sb.append("<br />");
		sb.append("<br />");
		sb.append("<font style='font-size:10px;'>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("</font>");
		sb.append("</span>");
		
		return sb.toString();
		

	}
	public static String getEmailAlertaMovimento(String placa, String cliente){
		StringBuilder sb = new StringBuilder();
		sb.append("<span style='font-size:20px; color: #2281CF;'>Prezado(a) "+cliente+",</span>");
		sb.append(		"<hr style='border: 1px #CDCDCD solid;'>");
		sb.append("<span style='font-size:12px; color:#555;'>");
		sb.append("	Foi identificado que o ve&iacute;culo placa <font style='color:#E77272'>"+placa+" </font>est&aacute; em movimento.");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<i>");
		sb.append("Cordialmente,");
		sb.append("<div>");
		sb.append("	<div>");
		sb.append("		<div>");
		sb.append("			<span style='font-size:x-small'>");
		sb.append("				<font color='#336666'><img src='http://www.trackhost.srv.br/Tracker/img/topo_login.png' width='96' height='49'><br></font>");
		sb.append("			</span>");
		sb.append("			<font color='#003333'>");
		sb.append("				<span style='font-size:x-small'>&nbsp;&nbsp;<a href='http://www.easytracking.com.br' target='_blank'>http://www.easytracking.com.br</a></span>");
		sb.append("			</font><br>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("</div>");
		sb.append("</i>");
		sb.append("<br />");
		sb.append("<br />");
		sb.append("<font style='font-size:10px;'>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("</font>");
		sb.append("</span>");
		
		return sb.toString();
		
		
	}
	public static String getEmailCombustivelCortado(String placa, String cliente){
		StringBuilder sb = new StringBuilder();
		sb.append("<span style='font-size:20px; color: #2281CF;'>Prezado(a) "+cliente+",</span>");
		sb.append(		"<hr style='border: 1px #CDCDCD solid;'>");
		sb.append("<span style='font-size:12px; color:#555;'>");
		sb.append("	Foi identificado que o ve&iacute;culo placa <font style='color:#E77272'>"+placa+" </font>est&aacute; com o combust&iacute;vel cortado.");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<br/ >");
		sb.append("<i>");
		sb.append("Cordialmente,");
		sb.append("<div>");
		sb.append("	<div>");
		sb.append("		<div>");
		sb.append("			<span style='font-size:x-small'>");
		sb.append("				<font color='#336666'><img src='http://www.trackhost.srv.br/Tracker/img/topo_login.png' width='96' height='49'><br></font>");
		sb.append("			</span>");
		sb.append("			<font color='#003333'>");
		sb.append("				<span style='font-size:x-small'>&nbsp;&nbsp;<a href='http://www.easytracking.com.br' target='_blank'>http://www.easytracking.com.br</a></span>");
		sb.append("			</font><br>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("</div>");
		sb.append("</i>");
		sb.append("<br />");
		sb.append("<br />");
		sb.append("<font style='font-size:10px;'>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>");
		sb.append("---------------------------------------------------------------------------------------------------------------------------------------------------------<br>");
		sb.append("</font>");
		sb.append("</span>");
		
		return sb.toString();
		
		
	}

}
