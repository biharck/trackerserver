package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="notificacaoeventoveiculo")
public class NotificacaoEventoVeiculo{

	private Integer id;
	private Veiculo veiculo;
	private Boolean emailVelocidade;
	private Boolean emailCorte;
	private Boolean emailAlertaMovimento;
	private Boolean enviarUnicaVez;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Boolean getEmailVelocidade() {
		return emailVelocidade;
	}

	public Boolean getEmailCorte() {
		return emailCorte;
	}

	public Boolean getEmailAlertaMovimento() {
		return emailAlertaMovimento;
	}

	public Boolean getEnviarUnicaVez() {
		return enviarUnicaVez;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public void setEmailVelocidade(Boolean emailVelocidade) {
		this.emailVelocidade = emailVelocidade;
	}

	public void setEmailCorte(Boolean emailCorte) {
		this.emailCorte = emailCorte;
	}

	public void setEmailAlertaMovimento(Boolean emailAlertaMovimento) {
		this.emailAlertaMovimento = emailAlertaMovimento;
	}
	public void setEnviarUnicaVez(Boolean enviarUnicaVez) {
		this.enviarUnicaVez = enviarUnicaVez;
	}
	
}
