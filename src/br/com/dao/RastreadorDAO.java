package br.com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.tracker.bean.Rastreador;
import br.com.util.HibernateUtil;

public class RastreadorDAO extends GenericDAO<Rastreador>{

	public Rastreador findBySerial(String serial){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Rastreador b = null;
		
		try {
			transaction = session.beginTransaction();
			b = (Rastreador) session.createQuery("from Rastreador WHERE idRastreador = "+serial).uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return b;
	}
}
