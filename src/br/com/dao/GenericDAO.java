package br.com.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.util.HibernateUtil;

public class GenericDAO<BEAN> {

	public Integer save(BEAN bean){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id = null;
		try {
			transaction = session.beginTransaction();
			id = (Integer) session.save(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public  List<BEAN> findAll(Class<BEAN> bean) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<BEAN> list = null;
		
		try {
			transaction = session.beginTransaction();
			list = session.createQuery("from "+bean.getName()).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public void delete(BEAN bean) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public static String[][] select(String sql, Session session) {
		List list = null;
		try {
			SQLQuery query = session.createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			list = query.list();
			 
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		String[][]array = new String[list.size()][];
		
		try {

			for (int i = 0; i < list.size(); i++) {
				Object[] array_tmp = ((Map) list.get(i)).values().toArray();
				array[i] = new String[array_tmp.length];
				for (int j = 0; j < array_tmp.length; j++) {
					array[i][j] = array_tmp[j].toString();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static boolean insertOrUpdate(String sql, Session session) {
		org.hibernate.Transaction tx = null;
		boolean result = false;
		try {
			tx = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}
}
