package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="veiculo")
public class Veiculo{
	
	private Integer id;
	private Rastreador rastreador;
	private Cliente cliente;
	private String placa;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	public Rastreador getRastreador() {
		return rastreador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	public String getPlaca() {
		return placa;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setRastreador(Rastreador rastreador) {
		this.rastreador = rastreador;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
