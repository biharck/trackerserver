package br.com.util;

import java.io.File;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtil {
	private String remetente;
	private String login;
	private String senha;
	private String smtp;
	private String porta;
	private String autenticacao;
	private String destinatario;
	private String assunto;
	private String corpo;
	private File anexo;
	protected Multipart multipart;
	protected MimeMessage message;

	public EmailUtil(String remetente, String login, String senha, String smtp,
			String porta, String autenticacao, String destinatario,
			String assunto, String corpo) {
		this.remetente = remetente;
		this.login = login;
		this.senha = senha;
		this.smtp = smtp;
		this.porta = porta;
		this.autenticacao = autenticacao;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;
	}

	public EmailUtil(String remetente, String login, String senha, String smtp,
			String porta, String autenticacao, String destinatario,
			String assunto, String corpo, File anexo) {
		this.remetente = remetente;
		this.login = login;
		this.senha = senha;
		this.smtp = smtp;
		this.porta = porta;
		this.autenticacao = autenticacao;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;
		this.anexo = anexo;
	}

	public EmailUtil() {
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getPorta() {
		return porta;
	}

	public void setPorta(String porta) {
		this.porta = porta;
	}

	public String getAutenticacao() {
		return autenticacao;
	}

	public void setAutenticacao(String autenticacao) {
		this.autenticacao = autenticacao;
	}

	public File getAnexo() {
		return anexo;
	}

	public void setAnexo(File anexo) {
		this.anexo = anexo;
	}

	public void addPart(MimeBodyPart part) throws Exception {
		// multipart = new MimeMultipart();
		multipart.addBodyPart(part);
	}

	public EmailUtil addHtmlText(String text) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text);
		messageBodyPart.setHeader("Content-Type", "text/html");
		addPart(messageBodyPart);
		return this;
	}


	public void sendMailGoogle() {
		// Sender's email ID needs to be mentioned
		String from = login;
		String pass = senha;
		// Recipient's email ID needs to be mentioned.
		String to = "biharck@gmail.com";
		String host = smtp;

		// Get system properties
		Properties properties = System.getProperties();
		// Setup mail server
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.user", from);
		properties.put("mail.smtp.password", pass);
		properties.put("mail.smtp.port", porta);
		properties.put("mail.smtp.auth", "true");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));

			// Set Subject: header field
			message.setSubject(assunto);
			
			MimeMultipart mpRoot = new MimeMultipart("mixed");
			MimeMultipart mpContent = new MimeMultipart("alternative");
			MimeBodyPart contentPartRoot = new MimeBodyPart();
			contentPartRoot.setContent(mpContent);
			mpRoot.addBodyPart(contentPartRoot);

			// enviando html
			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2 = new MimeBodyPart();
			mbp2.setText(corpo);
			mbp2.setHeader("Content-Type", "text/html");
			mpContent.addBodyPart(mbp2);

			message.setContent(mpRoot);
			message.saveChanges();

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("Sent message successfully....");
			
			
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
}
