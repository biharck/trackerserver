package br.com.easytracking.tracker.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
public class Cliente{
	
	
	private Integer id;
	private String razaoSocial;
	private String nomeFantasia;
	private String email1;
	private List<Veiculo> veiculos;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@OneToMany(mappedBy="cliente")
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail1() {
		return email1;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
}
