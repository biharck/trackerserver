package br.com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.tracker.bean.Veiculo;
import br.com.util.HibernateUtil;

public class VeiculoDAO extends GenericDAO<Veiculo> {

	
	public Veiculo findById(Integer id){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Veiculo b = null;
		
		try {
			transaction = session.beginTransaction();
			b = (Veiculo) session.createQuery("from Veiculo WHERE rastreador = "+id).uniqueResult();
			
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return b;
	}

}
