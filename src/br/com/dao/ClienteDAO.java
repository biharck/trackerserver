package br.com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.util.HibernateUtil;

public class ClienteDAO extends GenericDAO<Cliente>{
	
	
	public Cliente findById(Integer id){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Cliente b = null;
		
		try {
			transaction = session.beginTransaction();
			 Object result = session.createQuery("from Cliente c LEFT JOIN c.veiculos v WHERE v = "+id).uniqueResult();
			Object[] o = (Object[]) result;
			b = (Cliente) o[0];
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return b;
	}


}
