package br.com.easytracking.mail;

import br.com.dao.ClienteDAO;
import br.com.dao.EventosDAO;
import br.com.dao.NotificacaoEventoVeiculoDAO;
import br.com.dao.RastreadorDAO;
import br.com.dao.VeiculoDAO;
import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Eventos;
import br.com.easytracking.tracker.bean.NotificacaoEventoVeiculo;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.util.EmailUtil;
import br.com.util.EstruturaEmailUtil;

public class MailUtil {
	
	public static void sendMail(String serial, String evento, String date){
		
		
		//busca rastreador pelo serial
		RastreadorDAO dao = new RastreadorDAO();
		Rastreador r = dao.findBySerial(serial);
		if(r!=null){
			//busca veiculo pelo rastreador
			VeiculoDAO veiculoDAO = new VeiculoDAO();
			Veiculo v = veiculoDAO.findById(r.getId());
			if(v!=null){
				//busca evento pelo veiuclo
				//verifica se cliente quer que envie email baseado na situacao
				NotificacaoEventoVeiculoDAO eventoVeiculoDAO = new NotificacaoEventoVeiculoDAO();
				NotificacaoEventoVeiculo eventoVeiculo = eventoVeiculoDAO.findById(v.getId());
				if(eventoVeiculo!=null){
					if(evento.equals("velocidade_limite") && eventoVeiculo.getEmailVelocidade()){
						ClienteDAO clienteDAO = new ClienteDAO();
						Cliente c = clienteDAO.findById(v.getId());
						if(c!=null){
							sendMail("Excesso de Velocidade do Ve�culo "+v.getPlaca(), c.getEmail1(), EstruturaEmailUtil.getEmailVelocidadeLimite(v.getPlaca(),c.getRazaoSocial()),v, date);
						}
					}
					else if(evento.equals("alerta_movimento") && eventoVeiculo.getEmailVelocidade()){
						ClienteDAO clienteDAO = new ClienteDAO();
						Cliente c = clienteDAO.findById(v.getId());
						if(c!=null){
							sendMail("Alerta de Movimento do Ve�culo "+v.getPlaca(), c.getEmail1(), EstruturaEmailUtil.getEmailAlertaMovimento(v.getPlaca(),c.getRazaoSocial()),v,date);
						}
					}
					else if(evento.equals("combustivel_cortado") && eventoVeiculo.getEmailVelocidade()){
						ClienteDAO clienteDAO = new ClienteDAO();
						Cliente c = clienteDAO.findById(v.getId());
						if(c!=null){
							sendMail("Ve�culo com Combust�vel Cortado Placa "+v.getPlaca(), c.getEmail1(), EstruturaEmailUtil.getEmailCombustivelCortado(v.getPlaca(),c.getRazaoSocial()),v,date);
						}
					}
				}
			}
		}
	}
	
	private static void sendMail(String assunto,String destinatarios, String corpo, Veiculo v, String date){
		
		Eventos e = new Eventos();
		EventosDAO dao = new EventosDAO();
		e.setDataEvento(date);
		e.setMenssagem(assunto);
		e.setVeiculo(v);
		e.setVisto(false);
		dao.save(e);
		
		EmailUtil emailUtil = new EmailUtil("contato@easytracking.com.br", "contato@easytracking.com.br", 
				"abcd123.", "smtp.gmail.com", "587", "true", destinatarios, assunto, corpo);
		emailUtil.sendMailGoogle();
		
	}
}
