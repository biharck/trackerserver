package br.com.easytracking.tracker.position;

import br.com.dao.CoordenadasDAO;
import br.com.easytracking.mail.MailUtil;
import br.com.easytracking.tracker.bean.Coordenadas;
import br.com.util.TokenUtil;

public class ReceiverPosition {
	
	public void receiver(Coordenadas[] coordenadas, String token){
		if(token.equals(TokenUtil.getToken())){
			CoordenadasDAO dao = new CoordenadasDAO();
			for (Coordenadas c : coordenadas) {
				
				if(c.getMoving()!=null && c.getMoving().equals("1"))
					MailUtil.sendMail(c.getSerial(), "alerta_movimento", c.getDate());
				else if(c.getInput1()!=null && c.getInput1().equals("1"))
					MailUtil.sendMail(c.getSerial(), "combustivel_cortado",c.getDate());
				else if(c.getExcessSpeed()!=null && c.getExcessSpeed().equals("1"))
					MailUtil.sendMail(c.getSerial(), "velocidade_limite",c.getDate());
				
				System.out.println("Recebido id: " +c.getSerial());
				dao.save(c);
			}
		}else{
			System.out.println("Erro de Token de Confirmação");
		}
	}

}
