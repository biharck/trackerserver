package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rastreador")
public class Rastreador {
	
	private Integer id;
	private String imei;
	private String idRastreador;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public String getImei() {
		return imei;
	}
	public String getIdRastreador() {
		return idRastreador;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public void setIdRastreador(String idRastreador) {
		this.idRastreador = idRastreador;
	}

}
